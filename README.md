MaxModRes 0.21

This patch replace the ResAn analysis sofware that came before with Diphone Studio.

It uses the resonator~ & resdisplay objects of the CNMAT package.
Please install a fresh release of CNMAT object (Apple Silicon compatible) from their Github:
https://github.com/CNMAT/CNMAT-Externs/releases/tag/v1.0.4c 

modres 2.0.4 is distributed separatly. Please download "modres-2.0.4_MacOSX.dmg" and put "moderes-2.0.4" folder in "/Applications".

New in v0.21:

- Tested with new CNMAT Apple Silicon objects on an M1 computer
- modres and resloop run natively on M1 computers 

New in v0.2:

- Add dynamic naming depending on data changes.
- spat.shell replaced by spat5.shell
- Signed version of modres
- The patch should now work on Catalina and up.
